<?php

/*
 * Template Name: Contact
 */

get_template_part('parts/header'); the_post(); ?>

<main>
	
	<?php get_template_part('parts/page', 'header');?>

	<div class="contact padding--both white--bg">
		<svg class="contact__svg" viewBox="0 0 500 150" preserveAspectRatio="none" style="width: 100%;"><path d="M0.00,49.98 C149.99,150.00 349.20,-49.98 500.00,49.98 L500.00,150.00 L0.00,150.00 Z" style="stroke: none;"></path>
		</svg>

		<div class="wrap hpad">
			<div class="row flex flex--wrap">

				<div class="col-sm-6 contact__form">
				<?php gravity_form( 1, $display_title = true, $display_description = false, $display_inactive = false, $field_values = null, $ajax = false, 2, $echo = true ); 
				?>
				</div>

				<?php 
					$title = get_field('contact_title');
					$text = get_field('contact_text');
				 ?>

				<div class="col-sm-6 contact__info green--bg">
					<div class="contact__wrap">
						<h2 class="contact__title"><?php echo esc_html($title); ?></h2>
						<?php echo $text; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	

</main>

<?php get_template_part('parts/footer'); ?>
