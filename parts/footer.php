<?php $wave = get_field('wave_shape'); ?>

<?php if ($wave !== 'transparent') : ?>
<footer class="footer black--bg padding--bottom" id="footer">
<?php else : ?>
<footer class="footer" id="footer">
<?php endif; ?>
	<svg class="<?php echo esc_attr($wave); ?>--bg" viewBox="0 0 500 150" preserveAspectRatio="none" style="width: 100%;"><path d="M0.00,49.98 C149.99,150.00 349.20,-49.98 500.00,49.98 L500.00,150.00 L0.00,150.00 Z" style="stroke: none;"></path></svg>

	<?php if ($wave !== 'transparent') : ?>
	<div class="wrap hpad">
	<?php else : ?>
	<div class="wrap hpad black--bg padding--bottom footer__container">
	<?php endif; ?>
		<div class="row">
			<?php 
				if (have_rows('footer_columns', 'options') ) : while (have_rows('footer_columns', 'options') ) :
					the_row();
				$title = get_sub_field('column_title');
				$text = get_sub_field('column_text');
			 ?>

			 <div class="col-sm-3 col-sm-offset-1 footer__item">
			 	<h4 class="footer__title"><?php echo esc_html($title); ?></h4>
				<?php echo $text; ?>
			 </div>

			<?php endwhile; endif; ?>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/da_DK/sdk.js#xfbml=1&version=v3.3&appId=2216892175040699&autoLogAppEvents=1"></script>

</body>
</html>
