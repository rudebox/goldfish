<?php 
	if ( is_tax() ) {
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		$title = $term->name;
	} elseif ( is_category() ) {
		$title = single_cat_title( '', false );
	} elseif ( is_archive() ) {
		$title = post_type_archive_title( '', false );
	} else {
		$id = (is_home()) ? get_option('page_for_posts') : $post->ID;
		$title = get_proper_title($id);
	}

	//text
	$text = get_field('page_text');

	//product img
	$product = get_field('product_img');

	//figure img
	$figure = get_field('page_figure');
?>

<section class="page__hero">
		
	<?php if (is_front_page() ) : ?>
	<svg class="page__svg" viewBox="0 0 500 150" preserveAspectRatio="none" style="width: 100%;"><path d="M0.00,49.98 C149.99,150.00 349.20,-49.98 500.00,49.98 L500.00,150.00 L0.00,150.00 Z" style="stroke: none;"></path></svg>
	<?php endif; ?>

	<div class="wrap hpad page__container">

		<?php if ($figure) : ?>
			<div class="col-sm-6">
				<h1 class="page__title"><?php echo $title; ?></h1>
				<p><?php echo $text; ?></p>
			</div>
		<?php else : ?>
			<h1 class="page__title"><?php echo $title; ?></h1>
			<?php echo $text; ?>
		<?php endif; ?>
	
		
		<?php if ($figure) : ?>
			<div class="col-sm-6 wow bounceIn">
				<img class="page__figure" src="<?php echo $figure['url']; ?>" alt="<?php echo $figure['alt']; ?>">
			</div>
		<?php endif; ?>

		<?php if ($product) : ?>
		<div class="page__wrap col-sm-12 wow bounceIn">
			<img class="page__product" src="<?php echo $product['url']; ?>" alt="<?php echo $product['alt']; ?>">
		</div>
		<?php endif; ?>
	</div>
</section>