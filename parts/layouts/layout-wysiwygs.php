<?php 
/**
* Description: Lionlab WYSIWYGS repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//section settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$center = get_sub_field('center');
$wave = get_sub_field('wave_shape');
$img = get_sub_field('img');

if ($center === true) {
  $center = 'center';
}

global $layout_count; ?>
<?php if ($img) : ?>
<section id="wysiwygs-<?php echo $layout_count; ?>" class="wysiwygs wysiwygs--img <?php echo esc_attr($bg); ?>--bg padding--<?php echo esc_attr($margin); ?>" style="background-image: url(<?php echo $img['url']; ?>);">
<?php else : ?>
<section id="wysiwygs-<?php echo $layout_count; ?>" class="wysiwygs <?php echo esc_attr($bg); ?>--bg padding--<?php echo esc_attr($margin); ?>">
<?php endif; ?>

  <?php if ($wave != 'none') : ?>
  <svg class="<?php echo esc_attr($wave); ?>--bg" viewBox="0 0 500 150" preserveAspectRatio="none" style="width: 100%;"><path d="M0.00,49.98 C149.99,150.00 349.20,-49.98 500.00,49.98 L500.00,150.00 L0.00,150.00 Z" style="stroke: none;"></path></svg>
  <?php endif; ?>

  <div class="wrap hpad clearfix">
    <?php if(get_sub_field('header')): ?>
      <h2 class="wysiwygs__title <?php echo esc_attr($center); ?>"><?php echo esc_html(the_sub_field('header')); ?></h2>
    <?php endif; ?>
    <?php
    if(get_sub_field('offset') !== 'Flexible') {
        $flex = false;
        if(get_sub_field('offset') === '2 to 1') {
          $offset = '2:1';
        } else {
          $offset = '1:2';
        }
      } else {
        $flex = true;
        $offset = null;
      }
      scratch_layout_declare(get_sub_field('wysiwygs'), 2, $flex, $offset);
      while(has_sub_field('wysiwygs')) {
        scratch_layout_start();
          the_sub_field('wysiwyg');
        scratch_layout_end();
      }
    ?>
  </div>
</section>